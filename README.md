# README #

### What is this repository for? ###

* This is a WordPress plugin to customize the default login screen
* Version 1.0.0

### How do I get set up? ###

* Upload plugin to WordPress
* Activate plugin
* Plugin looks for login logo image at wp-content/plugins/eazy_login_logo/images/login-logo.png
* Image name for logo should be login-logo.png, default size is 84px x 84px



### Who do I talk to? ###

* Plugin information and documentation is available at http://robjscott.com/wordpress